
public class Aeropuerto {

	private String airportId;
	private String name;
	private String city;
	private String country;
	private String iata;
	private String icao;
	private double latitude;
	private double longitude;

	public Aeropuerto(String aeropuerto) {

		String[] parts = aeropuerto.split(",");
		this.airportId = parts[0];
		this.city = parts[2].substring(1, parts[2].length() - 1);
		this.country = parts[3].substring(1, parts[3].length() - 1);
		this.name = parts[1].substring(1, parts[1].length() - 1);
		this.iata = parts[4].substring(1, parts[4].length() - 1);
		this.icao = parts[5].substring(1, parts[5].length() - 1);
		this.latitude = Double.parseDouble(parts[6]);
		this.longitude = Double.parseDouble(parts[7]);

	}

	public String getAirportId() {
		return airportId;
	}

	public String getName() {
		return name;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getIata() {
		return iata;
	}

	public String getIcao() {
		return icao;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

}
