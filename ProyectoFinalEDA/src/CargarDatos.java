
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;


public class CargarDatos {
	Aeropuerto aeropuerto;
	Ruta ruta;
	private String cadena;
	private String cd;
	private String aeropuertos;
	private String rutas;
	File f;
	BufferedReader b;

	public CargarDatos(String erpuert, String ruts) {
		this.aeropuertos = erpuert;
		this.rutas = ruts;
		try {
			cargarArchivoAeropuertos(aeropuertos);
			cargarArchivoRutas(rutas);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void cargarArchivoAeropuertos(String ar) throws IOException {
		
		f = new File(ar);
		//Poniendo utf 8 para leer caracteres extra�os.
		b = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF8"));
		while ((cadena = b.readLine()) != null) {

			Listas.listaAeropuertos.add(aeropuerto = new Aeropuerto(cadena));
		}
		Listas.llenarOpciones();
		b.close();

	}

	private void cargarArchivoRutas(String ar) throws IOException {
		f = new File(ar);
		//Poniendo utf 8 para leer caracteres extra�os.
		b = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF8"));
		while ((cd = b.readLine()) != null) {
			Listas.listaRutas.add(ruta = new Ruta(cd));
		}

		b.close();
	}

}
