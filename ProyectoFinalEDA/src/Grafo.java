import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import org.graphstream.graph.implementations.SingleGraph;

public class Grafo {

	private String all = "url('src/ficheros/all.txt')";
	private String styleSheet = "url('src/ficheros/style.txt')";
	private Graph graph;

	public Grafo() {
		graph = new SingleGraph("Aeropuertos", false, true);
		graph.addAttribute("ui.stylesheet", styleSheet);

		cargarNodos();

		cargarAristas();
		borrarNodosSinRelaciones();
	}

	private void borrarNodosSinRelaciones() {

		// Obtener numero total de nodos
		int n = graph.getNodeCount();
		// Recorrer todos los nodos por cada nodo buscando Aristas
		for (int i = 0; i < n; i++) {

			boolean tiene = false;
			for (int j = 0; j < n; j++) {
				if ((graph.getNode(i).hasEdgeBetween(j))) {
					// Si tiene admenos una relacion sera true
					tiene = true;
				}
			}
			// Si ese nodo no tenia relacion con otro sera eliminado de la lista de
			// seleccion
			if (tiene == false) {
				Node borrar = graph.getNode(i);
				for (Aeropuerto aeropuerto : Listas.listaAeropuertos) {
					if (aeropuerto.getAirportId() == borrar.getId()) {
						// Se borra de la lista de seleccion de usuario cualquier nodo que no tenga
						// relacion con admenos otro nodo.
						Listas.aeropuertos.remove(aeropuerto.getName());

					}
				}
			}
		}

	}

	private void cargarNodos() {
		for (Aeropuerto aeropuerto : Listas.listaAeropuertos) {
			graph.addNode(aeropuerto.getAirportId()).addAttribute("xy", aeropuerto.getLongitude(),
					aeropuerto.getLatitude());
		}
	}

	private void cargarAristas() {
		int numAnterior = 0;
		for (Ruta rutas : Listas.listaRutas) {
			double longitud1 = 0;
			double latitud1 = 0;
			double longitud2 = 0;
			double latitud2 = 0;
			String sourcId = rutas.getSourceAirportId();
			String destiId = rutas.getDestinationAirportId();
			graph.addEdge(sourcId + destiId, sourcId, destiId);

			for (Aeropuerto aeropuerto : Listas.listaAeropuertos) {

				if (sourcId.equals(aeropuerto.getAirportId())) {
					latitud1 = aeropuerto.getLatitude();
					longitud1 = aeropuerto.getLongitude();
				} else if (aeropuerto.getAirportId().equals(destiId)) {
					latitud2 = aeropuerto.getLatitude();
					longitud2 = aeropuerto.getLongitude();
				}

			}
			int nEjes = graph.getEdgeCount() - 1;
			if (numAnterior == nEjes) {
				numAnterior++;

				graph.getEdge(nEjes).setAttribute("length",
						(distanciaDosPuntos(longitud1, latitud1, longitud2, latitud2)));
			}

		}

	}

	public void destinosPosibles(String origen, boolean acumular) {
		boolean acu = acumular;

		if (acu == false) {

			for (Node node : graph) {
				node.setAttribute("ui.class", "");
			}

			for (Edge edge : graph.getEachEdge()) {
				edge.setAttribute("ui.class", "");
			}

		}

		String imprimir = "";

		Node source = null;
		for (Aeropuerto aeropuerto : Listas.listaAeropuertos) {
			if (origen == aeropuerto.getName()) {
				source = graph.getNode(aeropuerto.getAirportId());

				imprimir += "Los destinos posibles sin escalas desde el aeropuerto: " + aeropuerto.getName() + " en "
						+ aeropuerto.getCity() + ", " + aeropuerto.getCountry() + " son:" + "\n";
			}
		}
		Iterator<Node> k = source.getNeighborNodeIterator();
		source.removeAttribute("ui.class");

		source.setAttribute("ui.class", "centered");

		while (k.hasNext()) {
			Node next = k.next();

			next.getEdgeFrom(source).setAttribute("ui.class", "marked");
			next.setAttribute("ui.class", "marked");

			for (Aeropuerto aeropuerto : Listas.listaAeropuertos) {
				if (next.getId() == aeropuerto.getAirportId()) {

					imprimir += "El aeropuerto: " + aeropuerto.getName() + " en " + aeropuerto.getCity() + ", "
							+ aeropuerto.getCountry() + "\n" + "A una distancia de "
							+ next.getEdgeFrom(source).getAttribute("length")
							+ " km y un tiempo aproximado de vuelo de "
							+ horasDeVuelo((next.getEdgeFrom(source).getAttribute("length"))) + "\n";
				}
			}
		}

		imprimir += "############################################################################################ \n";
		ResumenDatos.addtext(imprimir);
	}

	public void caminoMasCorto(String origen, String destino, boolean acumular) {
		boolean acu = acumular;

		if (acu == false) {

			for (Node node : graph) {

				node.setAttribute("ui.class", "");
			}

			for (Edge edge : graph.getEachEdge()) {
				edge.setAttribute("ui.class", "");

			}

		}

		Dijkstra dijkstra = new Dijkstra(Dijkstra.Element.EDGE, null, "length");
		dijkstra.init(graph);
		Node nodoOrigen = null;
		Node nodoDestino = null;
		String direccionOrigen = null;
		String direccionDestino = null;
		for (Aeropuerto aeropuerto : Listas.listaAeropuertos) {
			if (origen == aeropuerto.getName()) {
				nodoOrigen = graph.getNode(aeropuerto.getAirportId());
				direccionOrigen = aeropuerto.getCity() + ", " + aeropuerto.getCountry();

			}

			if (destino == aeropuerto.getName()) {
				nodoDestino = graph.getNode(aeropuerto.getAirportId());
				direccionDestino = aeropuerto.getCity() + ", " + aeropuerto.getCountry();

			}
		}

		dijkstra.setSource(nodoOrigen);
		dijkstra.compute();

		for (Node node : dijkstra.getPathNodes(nodoDestino)) {
			node.setAttribute("ui.class", "marked");
		}

		nodoOrigen.removeAttribute("ui.class");

		nodoOrigen.setAttribute("ui.class", "centered");

		nodoDestino.removeAttribute("ui.class");

		nodoDestino.setAttribute("ui.class", "destination");

		for (Edge edge : dijkstra.getPathEdges(nodoDestino)) {
			edge.setAttribute("ui.class", "marked");
		}
		Path camino = dijkstra.getPath(nodoDestino);
		// System.out.println(camino);

		Iterator<Node> k = camino.getNodeIterator();
		ArrayList<String> nodosCamino = new ArrayList<String>();
		double distancia = 0;

		while (k.hasNext()) {
			Node next = k.next();
			nodosCamino.add(next.getId());

		}
		int cantidad = 0;
		Node anterior = null;

		distancia = dijkstra.getPathLength(nodoDestino);
		distancia = (double) Math.round(distancia * 100d) / 100d;

		String imprimir = "";
		imprimir += ("Vuelo desde el aeropuerto: " + origen + " en " + direccionOrigen + "\n" + "Hasta el aeropuerto: "
				+ destino + " en " + direccionDestino + "\n" + "Con una distancia total de " + distancia
				+ " km y un tiempo total de vuelo de " + horasDeVuelo(distancia) + "\n" + "************" + "\n"
				+ "Itinerario" + "\n" + "****************************************************" + "\n");
		int parada = -1;
		for (String c : nodosCamino) {
			parada++;

			for (Aeropuerto aeropuerto : Listas.listaAeropuertos) {
				if (c == aeropuerto.getAirportId()) {
					if (nodosCamino.size() == 2 && parada == 0) {
						imprimir += ("Ruta directa no hay escalas!" + "\n"
								+ "****************************************************" + "\n");
					} else {
						if ((!(parada == (nodosCamino.size() - 1))) && parada != 0) {

							imprimir += ("En la parada n: " + parada + "\n" + "Se hara escala en el aeropuerto: "
									+ aeropuerto.getName() + " en " + aeropuerto.getCity() + ", "
									+ aeropuerto.getCountry() + "\n"
									+ "****************************************************" + "\n");
						} else {
							if (parada == 0) {

								imprimir += ("Salimos del aeropuerto: " + aeropuerto.getName() + " en "
										+ aeropuerto.getCity() + ", " + aeropuerto.getCountry()) + "\n";
							} else {
								imprimir += ("Parada Destino" + "\n" + "Aterrizamos en el aeropuerto: "
										+ aeropuerto.getName() + " en " + aeropuerto.getCity() + ", "
										+ aeropuerto.getCountry() + "\n");
							}
						}
					}
				}
			}
		}

		imprimir += "############################################################################################"
				+ "\n \n";

		ResumenDatos.addtext(imprimir);
	}

	public double distanciaDosPuntos(double longitud1, double latitud1, double longitud2, double latitud2) {
		int earth = 6377;
		double long1 = Math.toRadians(longitud1);
		double lat1 = Math.toRadians(latitud1);
		double long2 = Math.toRadians(longitud2);
		double lat2 = Math.toRadians(latitud2);
		double dlong = long2 - long1;
		double dlat = lat2 - lat1;
		double sinLat = Math.sin(dlat / 2);
		double sinLong = Math.sin(dlong / 2);
		double a = (sinLat * sinLat) + ((Math.cos(lat1)) * (Math.cos(lat2)) * (sinLong * sinLong));
		double c = 2 * (Math.asin(Math.min(1, Math.sqrt(a))));
		double distancia = earth * c;

		distancia = (double) Math.round(distancia * 100d) / 100d;

		return distancia;

	}

	static String horasDeVuelo(Object distancia) {

		String tiempo = "";
		int velocidad = 900;
		double distanciaAConvertir = (double) distancia;
		double horasSinConvertir = distanciaAConvertir / velocidad;
		double minutosSinConvertir = horasSinConvertir % 1;
		int hora = (int) (horasSinConvertir - minutosSinConvertir);
		int minuto = (int) (minutosSinConvertir * 60);

		if (hora == 1) {
			tiempo = "1 hora con ";
		} else if (hora > 1) {
			tiempo = hora + " horas y ";
		}

		tiempo += minuto + " minutos";

		return tiempo;
	}

	public void showAllGraph(Boolean show) {
		graph.removeAttribute("ui.stylesheet");
		if (!show) {
			graph.addAttribute("ui.stylesheet", all);
		} else if (show) {
			graph.addAttribute("ui.stylesheet", styleSheet);
		}

	}

	public Graph getGrafo() {
		return this.graph;
	}

}
