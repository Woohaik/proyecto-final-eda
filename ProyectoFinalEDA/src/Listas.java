import java.util.ArrayList;
import java.util.Collections;

public class Listas {

	static ArrayList<Aeropuerto> listaAeropuertos = new ArrayList<Aeropuerto>();
	static ArrayList<Ruta> listaRutas = new ArrayList<Ruta>();
	static ArrayList<String> aeropuertos = new ArrayList<String>();

	static void llenarOpciones() {
		for (Aeropuerto aeropuerto : listaAeropuertos) {
			aeropuertos.add(aeropuerto.getName());
		}
	}

	static ArrayList<String> getAeropuertos() {
		Collections.sort(aeropuertos);
		return aeropuertos;
	}

}
