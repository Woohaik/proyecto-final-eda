
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JPanel;


import org.graphstream.graph.Graph;

import org.graphstream.ui.graphicGraph.GraphicGraph;
import org.graphstream.ui.swingViewer.DefaultView;
import org.graphstream.ui.swingViewer.LayerRenderer;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;

public class PanelGrafo extends JPanel {
	
	Grafo grafo = new Grafo();
	Graph graph = grafo.getGrafo();
	
	

	public PanelGrafo(int w, int h) {
		setSize(w, h);
		this.setLayout(new GridLayout());

		Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);

		ViewPanel viewPanel = viewer.addDefaultView(false);

		DefaultView view = (DefaultView) viewPanel;

		view.getCamera().setViewPercent(0.905);

		view.setBackLayerRenderer(new LayerRenderer() {
			@Override
			public void render(Graphics2D graphics2D, GraphicGraph graphicGraph, double v, int i, int i1, double v1,
					double v2, double v3, double v4) {

				Image image = Toolkit.getDefaultToolkit().getImage("mundo.jpg");
				graphics2D.drawImage(image, 0, ((getHeight() - (int) h) / 2), (int) w,
						(int) h, null);
			}
		});

		this.add(view);
		/**/
	}
	
	
	public Grafo grafo() {
		return grafo;
	}
	
	
	
	


}
