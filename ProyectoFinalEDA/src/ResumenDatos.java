import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ResumenDatos extends JFrame {
	

	private ImageIcon imageIcon = new ImageIcon("text.png");

	private JPanel contentPane;
	static JTextArea textArea;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public ResumenDatos() {
		this.setIconImage(imageIcon.getImage());
		setTitle("Resumen");
		this.setResizable(false);
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 720, 648);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setFocusable(false);
		JScrollPane sbrText = new JScrollPane(textArea);
		sbrText.setBounds(10, 11, 694, 567);
		contentPane.add(sbrText);
		btnNewButton = new JButton("Limpiar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText(null);
			}
		});
		btnNewButton.setBounds(315, 589, 89, 23);
		contentPane.add(btnNewButton);

		textArea.setText(null);

	}

	static void addtext(String contenido) {
		if (!(textArea.getText() == null)) {
			textArea.setText(textArea.getText().trim() + "\n" + contenido);
		} else {
			textArea.setText(contenido);
		}

	}
}
