
public class Ruta {
	private String airline;
	private int airlineid;
	private String sourceAirport;
	private String sourceAirportId;
	private String destinationAirport;
	private String destinationAirportId;

	public Ruta(String ruta) {

		String[] parts = ruta.split(",");
		this.airline = parts[0];
		if (parts[1].contains("N")) {
			this.airlineid = -1;
		} else {
			this.airlineid = Integer.parseInt(parts[1]);
		}
		this.sourceAirport = parts[2];

		if (parts[3].contains("N")) {
			this.sourceAirportId = "-1";
		} else {
			this.sourceAirportId = parts[3];
		}

		this.destinationAirport = parts[4];
		if (parts[5].contains("N")) {
			this.destinationAirportId = "-1";
		} else {
			this.destinationAirportId = parts[5];
		}

	}

	public String getAirline() {
		return airline;
	}

	public int getAirlineid() {
		return airlineid;
	}

	public String getSourceAirport() {
		return sourceAirport;
	}

	public String getSourceAirportId() {
		return sourceAirportId;
	}

	public String getDestinationAirport() {
		return destinationAirport;
	}

	public String getDestinationAirportId() {
		return destinationAirportId;
	}

}
