import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JFrame;

import javax.swing.JPanel;

import javax.swing.JComboBox;

import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;

import java.awt.Color;

import javax.swing.JLabel;

import javax.swing.JOptionPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import java.awt.Button;
import javax.swing.JCheckBox;
import java.awt.Checkbox;
import javax.swing.JButton;

public class Window {

	private ImageIcon imageIcon = new ImageIcon("plane.png");

	private JFrame frmVentana;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frmVentana.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ResumenDatos rDatos = new ResumenDatos();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		CargarDatos cargarDatos = new CargarDatos("src/ficheros/airports.txt", "src/ficheros/routes.txt");
		Listas l = new Listas();

	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		double wid = 1290 / 0.921;
		double hei = 646 / 0.921;

		frmVentana = new JFrame();
		frmVentana.setIconImage(imageIcon.getImage());
		frmVentana.setTitle("Ventana");
		frmVentana.setBounds(100, 100, 1286, 750);
		frmVentana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmVentana.setLocationRelativeTo(null);
		frmVentana.setResizable(false);
		frmVentana.getContentPane().setLayout(null);

		JPanel panelGrafo = new PanelGrafo(1260, 629);

		panelGrafo.setSize(1260, 629);

		panelGrafo.setOpaque(true);
		panelGrafo.setBackground(new Color(255, 255, 255, 1));

		panelGrafo.setLocation(10, 10);

		frmVentana.getContentPane().add(panelGrafo);

		JPanel panelOpciones = new JPanel();
		panelOpciones.setBounds(10, 650, 1260, 77);
		frmVentana.getContentPane().add(panelOpciones);
		panelOpciones.setBackground(Color.GRAY);
		panelOpciones.setLayout(null);

		JComboBox opcionesUsuario = new JComboBox();

		opcionesUsuario.setModel(new DefaultComboBoxModel(
				new String[] { "Camino m\u00E1s Corto", "Destinos Posibles", "Mostrar todo" }));
		opcionesUsuario.setToolTipText("");
		opcionesUsuario.setBounds(33, 32, 291, 20);
		panelOpciones.add(opcionesUsuario);

		JComboBox aeropuerto1 = new JComboBox();
		aeropuerto1.setModel(new DefaultComboBoxModel(Listas.getAeropuertos().toArray()));
		aeropuerto1.setBounds(398, 32, 276, 20);

		panelOpciones.add(aeropuerto1);

		JComboBox aeropuerto2 = new JComboBox();
		aeropuerto2.setModel(new DefaultComboBoxModel(Listas.getAeropuertos().toArray()));
		aeropuerto2.setBounds(705, 32, 276, 20);
		panelOpciones.add(aeropuerto2);
		// por defecto para evitar que sea la misma opcion de aeropuerto este comienza
		// con un valor diferente.
		aeropuerto2.setSelectedIndex(1);

		JLabel lblEligeUnaOpcion = new JLabel("Elige una opcion");
		lblEligeUnaOpcion.setBounds(33, 11, 165, 14);
		panelOpciones.add(lblEligeUnaOpcion);

		JLabel lblAeropuertoOrigen = new JLabel("Aeropuerto Origen");
		lblAeropuertoOrigen.setBounds(398, 11, 136, 14);
		panelOpciones.add(lblAeropuertoOrigen);

		JLabel lblAeropuertoDestino = new JLabel("Aeropuerto Destino");
		lblAeropuertoDestino.setBounds(705, 11, 183, 14);
		panelOpciones.add(lblAeropuertoDestino);

		Button button = new Button("Ir");

		button.setBounds(1159, 23, 70, 25);
		panelOpciones.add(button);

		Checkbox acumular = new Checkbox("Acumular");
		acumular.setBounds(1060, 24, 95, 22);
		panelOpciones.add(acumular);

		// eventos
		////////////////////////////////////////////////////

		// Eventos para la eleccion de las opciones de los usuarios
		// Opcion 0 Vuelo normal camino mas corto entre dos puertos
		// opcion 1 Posibles destinos desde un aeropuerto
		opcionesUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (opcionesUsuario.getSelectedIndex() == 1) {
					lblAeropuertoDestino.setVisible(false);
					aeropuerto2.setVisible(false);
				} else if (opcionesUsuario.getSelectedIndex() == 0) {
					lblAeropuertoDestino.setVisible(true);
					aeropuerto2.setVisible(true);
					if (aeropuerto2.getSelectedIndex() == aeropuerto1.getSelectedIndex()) {
						if (aeropuerto1.getSelectedIndex() == 0) {
							aeropuerto2.setSelectedIndex(1);
						} else {
							aeropuerto2.setSelectedIndex(0);
						}
					}
				}
			}
		});

		// Evitando que puedan ser seleccionado el mismo aeropuerto en las dos opciones
		aeropuerto1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (((opcionesUsuario.getSelectedIndex() == 0)
						&& (aeropuerto2.getSelectedIndex()) == (aeropuerto1.getSelectedIndex()))) {

					if (aeropuerto1.getSelectedIndex() > 0) {
						aeropuerto1.setSelectedIndex(aeropuerto1.getSelectedIndex() - 1);
					} else {
						aeropuerto1.setSelectedIndex(aeropuerto1.getSelectedIndex() + 1);
					}
					JOptionPane.showMessageDialog(null, "No puedes seleccioanr el mismo aeropuerto >:v");
				}
			}
		});
		// Evitando que puedan ser seleccionado el mismo aeropuerto en las dos opciones
		aeropuerto2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (((opcionesUsuario.getSelectedIndex() == 0)
						&& (aeropuerto2.getSelectedIndex()) == (aeropuerto1.getSelectedIndex()))) {

					if (aeropuerto2.getSelectedIndex() > 0) {
						aeropuerto2.setSelectedIndex(aeropuerto1.getSelectedIndex() - 1);
					} else {
						aeropuerto2.setSelectedIndex(aeropuerto1.getSelectedIndex() + 1);

					}
					JOptionPane.showMessageDialog(null, "No puedes seleccioanr el mismo aeropuerto >:v");
				}
			}
		});

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (opcionesUsuario.getSelectedIndex() == 0) {

					((PanelGrafo) panelGrafo).grafo().caminoMasCorto((String) aeropuerto1.getSelectedItem(),
							(String) aeropuerto2.getSelectedItem(), acumular.getState());

				} else if (opcionesUsuario.getSelectedIndex() == 1) {

					((PanelGrafo) panelGrafo).grafo().destinosPosibles((String) aeropuerto1.getSelectedItem(),
							acumular.getState());
				}
			}
		});

		opcionesUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (opcionesUsuario.getSelectedIndex() == 2) {
					((PanelGrafo) panelGrafo).grafo().showAllGraph(false);
					button.setEnabled(false);
					acumular.setEnabled(false);

				} else {
					acumular.setEnabled(true);
					((PanelGrafo) panelGrafo).grafo().showAllGraph(true);
					button.setEnabled(true);
				}
			}
		});

		////////////////////////////////////////////////////
		// Cierre eventos

	}
}
